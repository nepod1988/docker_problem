FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /docker_realty
WORKDIR /docker_realty
ADD Gemfile .
ADD Gemfile.lock .
RUN bundle install
ADD . .
ENV RAILS_ENV production
CMD rails server