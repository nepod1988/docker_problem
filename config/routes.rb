Rails.application.routes.draw do
  resources :post_attachments
  devise_for :users
  root 'categories#index'
  # get '*unmatched_route', to: 'application#not_found'

  resources :categories, only: [:index, :show] do
    resources :realties, only: [:index, :show]
  end
end
