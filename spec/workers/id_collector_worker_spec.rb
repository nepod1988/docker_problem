require 'rails_helper'

describe IdCollectorWorker, '#perform', sidekiq: :inline, vcr: { cassette_name: '/diesel_topic' } do
  Sidekiq::Testing.inline!
  subject { described_class.perform_async }
  before { subject }

  it 'collect ids of ads' do
    expect($redis.smembers('topic_ids').count).to eq(40)
  end

  it 'all ids are uniq' do
    expect($redis.smembers('topic_ids')).to eql($redis.smembers('topic_ids').uniq)
  end
end
