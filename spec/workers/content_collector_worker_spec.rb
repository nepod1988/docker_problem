require 'rails_helper'

# describe ContentCollectorWorker, '#perform', sidekiq: :inline, vcr: {cassette_name: '/diesel_show_ads' } do
describe ContentCollectorWorker, '#perform', sidekiq: :inline, vcr: {cassette_name: '/diesel_show_ads_test' } do
  Sidekiq::Testing.inline!

  let(:flat1) { Realty.find_by(remote_id: 292312784) }
  let(:flat2) { Realty.find_by(remote_id: 292319145) }

  # subject { [292312784, 292319145].each { |id| described_class.perform_async(id) } }
  subject { [291921352].each { |id| described_class.perform_async(id) } }

  # before { subject }

  it 'check title' do
    subject
    binding.pry
    expect(flat1.title).to eq('Продаю 1к кв в рабочем городке на Некрасова/Гагарина. инд.1/3,48 м2, хороший ремонт, а/н. Прошу:33500$')
    expect(flat2.title).to eq('Срочно продаю 1 ком кв на Гоголя Московская.105 серия.Цена 31500$')
  end

  it 'check uploading photo' do
    expect(flat1.post_attachments.count).to eq(24)
    expect(flat2.post_attachments.count).to eq(18)
  end

  it 'check created data' do
    expect(flat1.remote_created_at).to eq('2018-09-04 09:32:30.000000000 +0000')
    expect(flat2.remote_created_at).to eq('2018-09-11 07:35:33.000000000 +0000')
  end

  it 'category must be Однокомнатные' do
    expect(flat1.category_id).to eq(1)
    expect(flat2.category_id).to eq(1)
  end

  it 'content must include' do
    expect(flat1.content).to include('индивидуальной планировки в рабочем городке в районе Некрасова/Гагарина. Квартира расположена на первом')
    expect(flat2.content).to include(' но очень теплая квартира,состояние квартиры среднее,1 лоджия застекленная из зала.Цена')
  end

  it 'content should not include "Прикрепленные изображения"' do
    expect(flat1.content).not_to include('прикрепленные изображения'.upcase)
    expect(flat2.content).not_to include('прикрепленные изображения'.upcase)
  end

  it 'content should not include "Сообщение отредактировал"' do
    expect(flat1.content).not_to include('cообщение отредактировал'.upcase)
    expect(flat2.content).not_to include('cообщение отредактировал'.upcase)
  end

  it 'check remote_id' do
    expect(flat1.remote_id).to eql(292312784)
    expect(flat2.remote_id).to eql(292319145)
  end

#   TO DO  на количество символов, на уже существующий айдишник
end