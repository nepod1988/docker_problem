# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(id: 1, email: 'bla1@bla.kg', password: '666666')
Category.create!(id: 1, name: 'Продам 1 комнатную квартиру')
Category.create!(id: 2, name: 'Продам 2 комнатную квартиру')
Category.create!(id: 3, name: 'Продам 3 комнатную квартиру')
Category.create!(id: 4, name: 'Продам 4 комнатную квартиру')