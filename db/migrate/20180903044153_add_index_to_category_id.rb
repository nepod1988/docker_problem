class AddIndexToCategoryId < ActiveRecord::Migration[5.1]
  def change
    add_index :realties, :category_id
  end
end
