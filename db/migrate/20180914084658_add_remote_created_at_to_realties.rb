class AddRemoteCreatedAtToRealties < ActiveRecord::Migration[5.1]
  def change
    add_column :realties, :remote_created_at, :datetime
  end
end
