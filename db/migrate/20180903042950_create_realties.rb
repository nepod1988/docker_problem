class CreateRealties < ActiveRecord::Migration[5.1]
  def change
    create_table :realties do |t|
      t.string :title
      t.integer :remote_id
      t.integer :category
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
