class ChangeCategoryToCategoryId < ActiveRecord::Migration[5.1]
  def change
    rename_column :realties, :category, :category_id
  end
end
