class Realty < ApplicationRecord
  validates :title, presence: true, uniqueness: { case_sensitive: false }

  before_destroy :remove_photos_from_folder

  belongs_to :user
  belongs_to :category
  has_many :post_attachments, dependent: :destroy

  accepts_nested_attributes_for :post_attachments

  scope :parser, -> { where('remote_id IS NOT NULL') }

  private

  def remove_photos_from_folder
    FileUtils.rm_rf("app/assets/images/diesel/#{self.remote_id}") if File.directory?("app/assets/images/diesel/#{self.remote_id}")
  end
end
