class Parser
  def initialize(remote_id)
    @remote_id = remote_id
    @source_url = "http://diesel.elcat.kg/index.php?showtopic=#{remote_id}"
    @all_img = []
  end

  attr_reader :remote_id, :source_url, :all_img

  def node
    html = open(source_url)
    Oga.parse_html(html)
  end

  def ads_created
    ads_title = node.css('.posted_info').first.children[1]
    ads_title=ads_title.attributes
    ads_title=ads_title[-1]
    ads_title.value
  end

  def get_first_comment_text
    post_source_id = node.css('.post_block').first.children[1].attributes.first.value
    id_first_post = "#post_id_#{post_source_id.split("entry")[1]}"
    content_source = node.css(id_first_post)
    first_comment_content = content_source.first.children[3].children[5].children[3]
    first_comment_content.css('.edit').remove
    content=first_comment_content.text.strip
    delete_fragments=["\n", "\t", "Прикрепленные изображения", "прикрепленные изображения", "cообщение отредактировал", "Сообщение отредактировал"," ", "  "]
    delete_fragments.each {|fragment|
      content=content.split(fragment)
      content=content.join(' ')}
    content
  end

  def get_ads_title
    ads_title = node.at_css "h1"
    content=ads_title.text.strip
    delete_fragments=["\n", " ", "  "]
    delete_fragments.each {|fragment|
      content=content.split(fragment)
      content=content.join(' ')}
    content
  end

  def get_ads_category_id
    return 1 if parse_category == "Однокомнатные"
    return 2 if parse_category == "Двухкомнатные"
    return 3 if parse_category == "Трёхкомнатные"
    return 4 if parse_category == "Сдаю"
  end

  def get_imgs_array
    post_source_id = node.css('.post_block').first.children[1].attributes.first.value
    id_first_post = "#post_id_#{post_source_id.split("entry")[1]}"
    content_source = node.css(id_first_post)
    first_comment_content = content_source.first.children[3].children[5].children[3]

    attach_wrap = []
    first_comment_content.children.each do |child|
      if child.respond_to?(:attributes) && child.attributes != []
        attach_wrap << child if child.attributes.first.value == "attach_wrap"
      end
    end

    unless attach_wrap.empty?
      imgs = attach_wrap[0].children[3].css('img')
      imgs.attribute('src').each do |img|
        @all_img << img.value
      end
    end
    @all_img.uniq
  end

  private

  def parse_category
    ads_category_source = node.css('.breadcrumb')
    bread_count = ads_category_source.first.children.count
    content_current_bread = ads_category_source.first.children[bread_count-4].text.strip
    splitted_bread = content_current_bread.split(' ')
    splitted_bread.last
  end
end


# arr = ["2477961", "285320278", "291777403", "291892722", "291990733", "292047664", "292107788", "292142872", "292186277", "292190614", "292202809", "292209336"]
# arr.each do |remote_id|
#   ContentCollectorWorker.new.perform(remote_id)
# end