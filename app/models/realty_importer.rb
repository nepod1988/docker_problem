class RealtyImporter
  def initialize(parser)
    @parser = parser
  end

  attr_reader :parser

  def import
    ActiveRecord::Base.transaction do
      realty = Realty.new
      realty.content = parser.get_first_comment_text
      realty.category_id = parser.get_ads_category_id
      realty.remote_created_at = parser.ads_created
      realty.title = parser.get_ads_title
      realty.remote_id = parser.remote_id
      realty.user_id = 1
      realty.save!

      create_photo(realty) if parser.get_imgs_array.present?
    end
  end

  private

  def create_photo(realty)
    imgs = parser.get_imgs_array

    FileUtils.mkdir_p("app/assets/images/diesel/#{parser.remote_id}") unless File.directory?("app/assets/images/diesel/#{parser.remote_id}")
    imgs.map do |img|
      path = img[-38..-11]
      File.open("app/assets/images/diesel/#{parser.remote_id}/#{path}.png", 'wb') do |fo|
        fo.write open(img).read
        PostAttachment.create!(realty_id: realty.id, path: path)
      end
    end
  end
end