class IdCollectorWorker
  include Sidekiq::Worker
  sidekiq_options retry: 2

  def perform
    # что буддет, если луп вернет ошибку?
    diesel_topics_id = %w(305 459 460 461) # TO DO . добавить номера разделов. , 459, 460, 461 ,  305
    diesel_topics_id.each do |topic|
      page_number = %w(2)  # и страниц  5IdCollectorWorker
      page_number.each do |page|
        sleep 1  # что бы не забанили. После тестов увеличить интервал до 10
        url = "http://diesel.elcat.kg/index.php?showforum=#{topic}&prune_day=100&sort_by=Z-A&sort_key=last_post&topicfilter=all&page=#{page}"
        html = open(url)
        doc = Nokogiri::HTML(html)
        doc.css('.topic_title').map do |showing|
          parts = showing.attributes['href'].value.split("=")
          $redis.sadd('topic_ids', parts.last) unless $redis.sismember('topic_ids', parts.last)
        end
      end
    end
  end
end

