class RealtiesController < ApplicationController
  before_action :set_realty, only: [:show, :edit, :update, :destroy]

  def index
    @realties = Realty.parser.where(category_id: params[:category_id]).paginate(page: params[:page], :per_page => 10)
    # @realties = Realty.parser.all.paginate(page: params[:page])
  end

  def show
    if @realty
      @post_attachments = @realty.post_attachments.all
    else
      render 'not_found'
    end
  end

  # def cat
  #   case request.fullpath
  #   when "/realties/sale/1room"
  #     category_id = [1]
  #   when "/realties/sale/2room"
  #     category_id = [2]
  #   when "/realties/sale/3room"
  #     category_id = [3]
  #   when "/realties/sale"
  #     category_id = [1,2,3]
  #   else
  #     render 'not_found'
  #   end
  #   @realties = Realty.parser.where(category_id: category_id).paginate(page: params[:page], :per_page => 10)
  #   render 'index'
  # end

  def render_404
    render file: "#{Rails.root}/public/404", status: :not_found
  end


  # def new
  #   @realty = Realty.new
  #   @post_attachment = @realty.post_attachments.build
  # end

  # def edit
  #   @post_attachment = @realty.post_attachments.build
  # end

  # def create
  #   @realty = Realty.new(realty_params)
  #   @realty.user_id = current_user.id
  #   respond_to do |format|
  #     if @realty.save
  #       params[:post_attachments]['avatar'].each do |a|
  #         @post_attachment = @realty.post_attachments.create!(:avatar => a, :realty_id => @realty.id)
  #       end
  #       format.html { redirect_to @realty, notice: 'Realty was successfully created.' }
  #       format.json { render :show, status: :created, location: @realty }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @realty.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # def update
  #   respond_to do |format|
  #     if @realty.update(realty_params)
  #       if params[:post_attachments].present?
  #         params[:post_attachments]['avatar'].each do |a|
  #           @post_attachment = @realty.post_attachments.create!(:avatar => a, :realty_id => @realty.id)
  #         end
  #       end
  #
  #       format.html { redirect_to @realty, notice: 'Realty was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @realty }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @realty.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # def destroy
  #   @realty.destroy
  #   respond_to do |format|
  #     format.html { redirect_to realties_url, notice: 'Realty was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    def set_realty
      @realty = Realty.find_by(remote_id: params[:id])
    end

    def realty_params
      params.require(:realty).permit(:title, :remote_id, :category_id, :content, :user_id, post_attachments_attributes: [:id, :realty_id, :avatar])
    end
end
