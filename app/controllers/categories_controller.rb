class CategoriesController < ApplicationController
  def index
    # @categories = Category.all
    @realties = Realty.parser.all.paginate(page: params[:page], :per_page => 10)
    @title = 'Объявления о продаже и сдаче квартир'
    render 'realties/index'
  end

  def show
    @realties = Realty.parser.where(category_id: params[:id]).paginate(page: params[:page], :per_page => 10)
    @title = get_title
    render 'realties/index'
  end

  def get_title
    category = Category.find(params[:id])
    category.name
  end
end
