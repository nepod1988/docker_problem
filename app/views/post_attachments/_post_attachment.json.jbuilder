json.extract! post_attachment, :id, :realty_id, :avatar, :created_at, :updated_at
json.url post_attachment_url(post_attachment, format: :json)
