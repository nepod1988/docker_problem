module RealtiesHelper
  def get_photo_path(post)
    @realty.remote_id.present? ? "diesel/#{@realty.remote_id}/#{post.path}.png" : post.avatar
  end
end